<?php
/**
 * Created by PhpStorm.
 * User: neov
 * Date: 22.09.14
 * Time: 19:27
 */
?>
{{ Form::open(array('route'=> 'admin.posts.store')) }}
<ul>
    <li>
        {{ Form::label('user_id', 'Author') }}
        {{ Form::select('user_id', User::lists('name', 'id')) }}
        {{ $errors->first('user_id', '<p class="error">:message</p>') }}
    </li>
      <li>
            {{ Form::label('title', 'Title') }}
            {{ Form::text('title')}}
            {{ $errors->first('user_id', '<p class="error">:message</p>') }}
        </li>
        <li>
           {{ Form::label('body', 'Body') }}
           {{ Form::textarea('body')}}
           {{ $errors->first('body', '<p class="error">:message</p>') }}
        </li>
        <li>

          {{ Form::submit('Save')}}

        </li>
</ul>
{{ Form::close() }}

@stop