<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Admin panel></title>
{{HTML::style('css/admin.css')}}
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>
<body>
  <header>
      <div class="container">
        <h1>Admin panel</h1>
      </div>
  </header>
  <main class="container">
        @yield('content')
  </main>
  <footer>
      <div class="container">
        &copy; {{ date('Y') }} V24lab
      </div>
      </footer>
</body>
</html>