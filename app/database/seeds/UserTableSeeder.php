<?php
/**
 * Created by PhpStorm.
 * User: neov
 * Date: 21.09.14
 * Time: 20:46
 */
use Faker\Factory as Faker;

class UserTableSeeder Extends Seeder {
     public function run(){
         $faker = Faker::create();
         foreach(range(1,5) as $index)
         {
             User::create(
                 [
                     'email'=> $faker->email,
                     'name' => $faker->name,
                     'password' => Hash::make('tutsplus'),

                 ]
             );
         }

     }

} 